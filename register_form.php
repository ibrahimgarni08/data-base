<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
    <link rel="stylesheet" href="login_form.css">
</head>
<body>
    <header>
    <h1>Enregistrement</h1>
    <p>Pour vous inscrire, completez les informations suivantes.</p>
    </header>
    <main>
    <?php
    // Pas très beau, mais fonctionnel ! Challenge : trouvez comment faire mieux !!!
    if(!empty($_GET["error"])) {
        echo "<div class='error'>";
        switch($_GET["error"]) {
            case "empty" : echo "Tous les champs doivent être renseignés"; break; 
            case "password" : echo "Les mots de passes doivent être identiques"; break; 
            case "password_2" : echo "Le mot de passe doit contenir 8 caractères minimum, 1 majuscule, 1 minuscule, 1 chiffre et 1 caractère spécial"; break; 
            case "email" : echo "L'email est mal formé"; break; 
            case "duplicate" : echo "Cette adresse email est déjà enregistrée"; break; 
            break; 
        }
        echo "</div>";
    }
    ?>
    <form action="register.php" method="post">
        <p>
            <label for="email">Email : </label><input type="email" name="email"  required>
        </p>
        <p>
            <label for="password">Password : </label>
            <input type="password" name="password"  required >
        </p>
        <p>
            <label for="conf_password">Confirmation : </label><input type="password" name="conf_password" required />
        </p>
        <button type="submit">S'inscrire</button>
    </form>
    </main>
</body>
</html>