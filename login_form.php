<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="login_form.css">
    <title>Connexion</title>
</head>
<body>
    <header>
        <h1>Users INFOTECH</h1>
        <p>Bienvenue sur le réseau de la classe Infotech ! Connectez vous.</p>
    </header>
    <main>
        <?php
        if(!empty($_GET["status"])) {
            if($_GET["status"] == "registered") {
                echo "<div class='success'>Enregistrement réussi</div>";
            } else{
                if($_GET["status"] == "disconnected"){
                    echo "<div class='disconnected'>Vous avez bien été déconnecté ! </div>";
                } 
            }
        }
        ?>
        <form action="login.php" method="post">
            <p><label for="email">Email : </label><input type="email" name="email" required></p>
            <p><label for="password">Password : </label><input type="password" name="password" required></p>
            <button type="submit">Connexion</button>
        </form>
        <br>
        <a href="register_form.php">Pour vous inscrire, cliquez ici !</a>
        </main>
</body>
</html>