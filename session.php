<?php

if (session_status()!=SESSION_ACTIVE) {
    session_start();
}

if(empty($_SESSION["email"])) {
    header('Location: login.php');
    exit;
}

$email = $_SESSION["email"] ;


echo "Bonjour $email. Je t'ai créé la session : " . session_id() . " ";


?>