<?php
// TODO : Ze Job

/**
 * Redirige vers le formulaire d'enregistrement, en donnant en paramètre (GET) le type d'erreur 
 * 
 * Les types d'erreurs sont : 
 *  - empty : un des champs est vide
 *  - email : l'email est mal formée
 *  - password : le mot de passe ne correspond pas à sa confirmation
 * @param string $error La clé de l'erreur que l'on renvoie
 * @return void
 */
function error(string $error ) {
    header("Location: login_form.php?status=error&error=$error");
    // quitter
    exit; 

}

// Étape 1 : récupérer les données de formulaires : $_POST
$email = $_POST["email"]; 
$password = $_POST["password"];

// Étape 2 : connexion à la base 

try {
    include_once('bdd.php');
    $bdd_options = ["PDO::ATTR_ERR_MODE" => PDO::ERRMODE_EXCEPTION];
    $bdd = new PDO("mysql:host=localhost;dbname=$db_name;port=$db_port", $db_user, $db_pass, $bdd_options); 
} catch(Exception $e) {
    // On affiche les erreurs relative à la BDD SEULEMENT EN DEV!!!!!!
    echo $e->getMessage();
    http_response_code(500);
    exit; 
}

// Étape 3 récupération de l'utilisateur (*) à partir de son email 

$rqt = "SELECT * FROM utilisateur WHERE email=:email"; 
try {

    $requete_preparee = $bdd->prepare($rqt); 
    $requete_preparee->bindParam(':email', $email); 
    $requete_preparee->execute(); 
    $user = $requete_preparee->fetch(PDO::FETCH_ASSOC);
} catch(Exception $e) {
    // On affiche les erreurs relative à la BDD SEULEMENT EN DEV!!!!!!
    echo $e->getMessage();
    http_response_code(500);
    exit; 
}

if(empty($user)) {
    error("invalid");
}

// Étape 4 : vérification du mot de passe 
$hash = $user["password"]; 

if(!password_verify($password, $hash)) {
    // 4.1 : si erreur on lui indique 
    error("invalid");
}

    // 4.2 : si ok on l'envoi sur une page de succes (genre dashboard)
    header('Location: dashboard.php');
    session_start();
    $_SESSION['email'] = $email;


    