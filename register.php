<?php 

/**
 * Redirige vers le formulaire d'enregistrement, en donnant en paramètre (GET) le type d'erreur 
 * 
 * Les types d'erreurs sont : 
 *  - empty : un des champs est vide
 *  - email : l'email est mal formée
 *  - password : le mot de passe ne correspond pas à sa confirmation
 *
 * @return void
 */
function error(string $error ) {
    header("Location: register_form.php?error=$error");
    // quitter
    exit; 
}

    // 1-  Traiter les champs de formulaire
    if( empty($_POST['email']) || empty($_POST['password']) || empty($_POST['conf_password'])) {
        // Informer que les champs sont vides 
        error("empty");
    } 
    $email = $_POST['email']; 
    $password = $_POST['password'];
    $conf = $_POST['conf_password']; 

    // 1.1 - Valider la conformité de l'email
    if(!filter_var($email, FILTER_VALIDATE_EMAIL )) {
        error("email");
    }
    // 1.1.2 - Valider la conformité du mot de passe
    if (!preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W){8,}#', $password)) {
        error("password_2");
	}
    // 1.2 - vérifier le que la confirmation est correcte
    if($_POST["password"] !== $_POST["conf_password"]) {
        error("password"); 
    }
    // 1.3 - hasher le mot de passe 
    $hash = password_hash($password, PASSWORD_DEFAULT);

    // 2- Si erreur, informer l'utilisateur de l'erreur 
    //Déjà fait plus haut.

    // Connexion à la base de donnée : 
    require_once('bdd.php');
    try {
        $bdd_options = ["PDO::ATTR_ERR_MODE" => PDO::ERRMODE_EXCEPTION];
        $bdd = new PDO("mysql:host=localhost;dbname=$db_name;port=$db_port", $db_user, $db_pass, $bdd_options); 

    } catch(Exception $e) {
        echo $e->getMessage();
        exit;
    }


    // Préparation de la requête d'insertion dans la base de données

    $rqt = "INSERT INTO utilisateur(email, password) VALUES (:email, :password);"; 

    try {
        $requete_preparee = $bdd->prepare($rqt); 
    
        // Associer les paramètres : 
        $requete_preparee->bindParam(":email", $email); 
        $requete_preparee->bindParam(':password', $hash); 
        $requete_preparee->execute();
    } catch (Exception $e) {
        
        if($e->getCode() == 23000 ) { // Le code 23000 correspond à une entrée dupliquée :cela signifie que l'adresse mail est déjà en bdd
            error("duplicate");
        }

    }

    header('Location: PHP_Ibrahim.php?status=registered');

    