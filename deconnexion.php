<?php

session_unset();
session_destroy(); 

setcookie(session_name(), '', strtotime("-1 day"));
header('Location: login_form.php?status=disconnected');
exit;

?>